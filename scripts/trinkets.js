'use strict'
let trinkets = {};
let gear = {};
let fingers = {};
let karaTrinkets = ["142164", "142159", "142167", "142160", "142165", "142157"]
let legendaries = ["132376", "137014", "140846", "132443", "137024", "137084", "137102", "132445", "132460", "137072", "137038", "132409", "137060", "137052", "133977", "137017", "137101", "137088", "137019", "137086", "137040", "133976", "137066", "132442", "133800", "132863", "137100", "137018", "132441", "137097", "137041", "137030", "137074", "137095", "137028", "137015", "137036", "137067", "137616", "132447", "137026", "132861", "137045", "137050", "132456", "137027", "137051", "138854", "137043", "137063", "137079", "137099", "137047", "137057", "132394", "137046", "137039", "137049", "137085", "137044", "137065", "137108", "137029", "132374", "137053", "138949", "132366", "132454", "132411", "133974", "137068", "137048", "137022", "137056", "132407", "141353", "138140", "132864", "137107", "137220", "132406", "137090", "132437", "132450", "132451", "137276", "137087", "137034", "137104", "132455", "137076", "132375", "137092", "138879", "132459", "137096", "132449", "132357", "132457", "137058", "137083", "137023", "132444", "137227", "137061", "132453", "132393", "132413", "137080", "132466", "137071", "132378", "137016", "137223", "132452", "132367", "132365", "137032", "132410", "141321", "132379", "132436", "137025", "138117", "137103", "132381", "137075", "137042", "137382", "137078", "137035", "137091", "137021", "137062", "132448", "137064", "133973", "137054", "137094", "137031", "137089", "132458", "137070", "137059", "137033", "137073", "137105", "137037", "137081", "137077", "137020", "132369", "137069", "137109", "132461", "133970", "133971", "137055", "137098", "144281", "144293", "144280", "144249", "144259", "144279", "144292", "144295", "144354", "144432", "144242", "144258", "144326", "144303", "144361", "144274", "144355", "144260", "144277", "144340", "144239", "144273", "144275", "144358", "144244", "144247", "144438", "144236", "143732", "144364", "144385", "144369", "143728"];
let karaChests = {none:"", leather:"harness_of_smoldering_betrayal,id=142203,ilevel=875", cloth:"robes_of_the_ancient_chronicle,id=142297,ilevel=875", plate:"chestplate_of_impenetrable_darkness,id=142303,ilevel=875", mail:"hauberk_of_warped_intuition,id=142301,ilevel=875"};
let finalInnerHTML = '';

function selectValue(args, key) {
	/// Looks for the value of "key" inside string "args" and returns it.
	/// args looking like "ethereal_urn,id=142166,bonus_id=3454"
	/// Function will return "142166" of key "id" and "3454" for key "bonus_id"
	let start = args.indexOf(key);
	let result = null;
	if (start != -1) {
		let equal = args.indexOf('=', start);
		if (equal != -1) {
			let virgule = args.indexOf(',', equal);
			let amp = args.indexOf('&', equal);
			let end = -1;
			if ((virgule >= 0) && (amp >= 0)) {
				end = Math.min(virgule, amp);
			} else {
				end = Math.max(virgule, amp);
			}
			if (end == -1) {
				result = args.substr(equal+1);
			} else {
				result = args.substr(equal+1, end-equal-1);
			}
		}
	}
	return result;
}

function selectResults() {
	/// This function just perform a "select all" on the results' section when it's called
	let range = document.createRange();
	let selection = window.getSelection();
	range.selectNodeContents(document.getElementById('result'));

	selection.removeAllRanges();
	selection.addRange(range);
	return;
}

function replaceChest(keydown) {
	/// Main function that acts like a switch, read input then display it based on selection.
	let selector = document.getElementById('chestType');
	let chest = document.getElementById('chest');
	chest.value = karaChests[selector.value];
	return;
}

function main(output) {
	/// Main function that acts like a switch, read input then display it based on selection.
	finalInnerHTML = '';
	let resultField = document.getElementById('result');
	resultField.innerHTML = '';
	trinkets = {};
	fingers = {};
	gear = {};
	if (document.getElementById('trinkets').value != '') {
		convertTrinkets();
	}
	if (document.getElementById('gear').value != '') {
		convertGear();
	}
	if (output == 'singletons') {
		createListSingles();
	} else if (output == 'pairs') {
		createListPairs();
	} else if (output == 'export') {
		createListExport();
	}
	resultField.innerHTML = finalInnerHTML;
	saveCache();
	return;
}

function convertTrinkets() {
	/// Reads the user input line by line and convert it to a JavaScript Object
	let trinketsReader = document.getElementById('trinkets');
	let lines = trinketsReader.value.split('\n');
	let wowheadRegex = /wowhead.com\/item=(\d+)(?:\/([^&]+))?(?:&bonus=([:\d]+))?/
	let simCRegex = /(?:trinket\d=)?([^,]*),([^\s]+)/
	for (let i = 0 ; i < lines.length ; i++) {
		let line = lines[i].replace(/\s/g, '');
		let result = null;
		let id = null;
		let name = null;
		let bonus_id = null;
		let designation = null;
		let split = line.split('#');
		if (split.length > 1) {
			designation = split[2];
			if (split[0] != '') {
				line = split[0];
			} else {
				line = split[1];
			}
		}
		if (wowheadRegex.test(line)) {
			result = wowheadRegex.exec(line);
			if (selectValue(result[0], 'bonus')) {
				finalInnerHTML += 'You need to input a bonus id ' + line;
				return;
			}
			id = result[1];
			name = result[2].replace(/-/g, '_');
			bonus_id = selectValue(line, 'bonus').replace(/:/g, '/');
			designation = designation || name + '_' + bonus_id;
			trinkets[designation] = {};
			trinkets[designation]['id'] = id;
			trinkets[designation]['string'] = name + ',id=' + id + ',bonus_id=' + bonus_id;
		} else if (simCRegex.test(line)) {
			result = simCRegex.exec(line);
			if (result.length != 3) {
				resultField.innerHTML = 'String ' + line + ' is not valid.';
				return;
			}
			let args = result[2];
			let suffix = selectValue(args, 'ilevel') || selectValue(args, 'bonus_id');
			id = selectValue(args, 'id');
			name = result[1];
			let legendary = 0;
			if (legendaries.includes(id)) {
				legendary = 1;
			}
			if (!suffix || !id) {
				finalInnerHTML += 'You must provide an id and either an ilevel or bonus_id : ' + line;
				return;
			}
			designation = designation || (name || id.toString()) + '_' + suffix;
			trinkets[designation] = {};
			trinkets[designation]['id'] = id;
			trinkets[designation]['legendary'] = legendary;
			trinkets[designation]['string'] = name + ',' + args;
		}
	}
}

function convertGear() {
	/// Reads the user input line by line and convert it to a JavaScript Object
	let gearReader = document.getElementById('gear');
	let simCRegex = /([a-z]+)\d?=([^,]*),([^\s]+)/
	let fingerRegex = /(?:ring|finger)\d?=(.+)/
	let lines = gearReader.value.split('\n');
	for (let i = 0 ; i < lines.length ; i++) {
		let line = lines[i].replace(/\s/g, '');
		let result = null;
		let id = null;
		let name = null;
		let type = null;
		let bonus_id = null;
		let designation = null;
		let split = line.split('#');
		if (split.length > 1) {
			designation = split[2];
			if (split[0] != '') {
				line = split[0];
			} else {
				line = split[1];
			}
		}	
		if (simCRegex.test(line)) {
			result = simCRegex.exec(line);
			if (result.length != 4) {
				finalInnerHTML += 'String ' + line + ' is not valid.';
				return;
			}
			type = result[1];
			name = result[2];
			let args = result[3];
			let suffix = selectValue(args, 'ilevel') || selectValue(args, 'bonus_id');
			id = selectValue(args, 'id');
			name = result[2];
			let legendary = 0;
			if (legendaries.includes(id)) {
				legendary = 1;
			}
			if (!suffix || !id) {
				finalInnerHTML += 'You must provide an id and either an ilevel or bonus_id : ' + line;
				return;
			}
			designation = designation || (name || id.toString()) + '_' + suffix;
			if ((type == 'ring') || (type == 'finger')) {
				fingers[designation] = {};
				fingers[designation]['id'] = id;
				fingers[designation]['legendary'] = legendary;
				fingers[designation]['string'] = fingerRegex.exec(result[0])[1];
			} else {
				if (typeof gear[type] == "undefined") {
					gear[type] = {};
				}
				gear[type][designation] = {};
				gear[type][designation]['id'] = id;
				gear[type][designation]['legendary'] = legendary;
				gear[type][designation]['string'] = name + ',' + args;
			}
		}
	}
}

function createListSingles() {
	/// Creates a list of SimC trinkets, one at a time
	let nbTrinkets = Object.keys(trinkets).length;
	let nbGear = Object.keys(gear).length;
	let nbFingers = Object.keys(fingers).length;
	if ((nbTrinkets > 0) && ((nbGear == 0) && (nbFingers == 0))) {
		let chest = document.getElementById('chest').value;
		for (let i = 0 ; i < nbTrinkets ; i++) {
			let key = Object.keys(trinkets)[i];
			let bloc = 'copy=' + key + '</br>';
			bloc += 'trinket1=' + trinkets[key].string + '</br></br>';
			finalInnerHTML += bloc;
			if (karaTrinkets.includes(trinkets[key].id) && chest != '') {
				let karaChest = 'karaChest' + selectValue(chest, 'ilevel');
				finalInnerHTML += 'copy=' + key + '_' + karaChest + '</br>';
				finalInnerHTML += 'trinket1=' + trinkets[key].string + '</br>';
				finalInnerHTML += 'chest=' + chest + '</br></br>';
			}
		}
	} else if (((nbGear > 0) || (nbFingers > 0)) && (nbTrinkets == 0)) {
		let title = 'copy=';
		let innerHTML = '';
		let nbLeggos = 0;
		if (Object.keys(fingers).length !== 0 && fingers.constructor === Object) {
			handleFingers(title, innerHTML, nbLeggos);
		} else {
			handleGear(0, title, innerHTML, nbLeggos);
		}
	} else {
		finalInnerHTML += 'To use Singletons option, you should provide either trinkets or gear but not both or none.';
	}
	return;
}

function createListPairs() {
	/// Creates a list of pairs of SimC trinkets, will pair all the trinkets together except when both trinkets are the same (based on the item id)
	let chest = document.getElementById('chest').value;
	let nbTrinkets = Object.keys(trinkets).length;
	if (nbTrinkets < 2) {
		finalInnerHTML += 'You must provide at least 2 trinkets.</br>If you want to only generate combinations of gear, use the Singles option.';
		return;
	}
	for (let trinket1 = 0 ; trinket1 < nbTrinkets ; trinket1++) {
		for (let trinket2 = trinket1 + 1 ; trinket2 < nbTrinkets ; trinket2++) {
			let title = '';
			let innerHTML = '';
			let nbLeggos = 0;
			let karaChest = '';
			let keyTrinket1 = Object.keys(trinkets)[trinket1];
			let keyTrinket2 = Object.keys(trinkets)[trinket2];
			if (trinkets[keyTrinket1].id != trinkets[keyTrinket2].id) {
				nbLeggos += trinkets[keyTrinket1].legendary + trinkets[keyTrinket2].legendary;
				title += 'copy=' + keyTrinket1 + '+' + keyTrinket2;
				innerHTML += 'trinket1=' + trinkets[keyTrinket1].string + '</br>';
				innerHTML += 'trinket2=' + trinkets[keyTrinket2].string + '</br>';
				if ((karaTrinkets.includes(trinkets[keyTrinket1].id) || karaTrinkets.includes(trinkets[keyTrinket2].id)) && chest != '') {
					if (typeof gear["chest"] == "undefined") {
						gear["chest"] = {};
					}
					karaChest = "karaChest" + selectValue(chest, 'ilevel');
					gear["chest"][karaChest] = {};
					gear["chest"][karaChest]["id"] = selectValue(chest, 'id');
					gear["chest"][karaChest]["legendary"] = 0;
					gear["chest"][karaChest]["string"] = "chest=" + chest;
				} else if (typeof karaChest != "undefined" && karaChest != '') {
					delete gear.chest[karaChest];
				}
				if (Object.keys(fingers).length !== 0 && fingers.constructor === Object) {
					handleFingers(title, innerHTML, nbLeggos);
				} else {
					handleGear(0, title, innerHTML, nbLeggos);
				}
			}
		}
	}
	return;
}

function handleFingers(title, innerHTML, nbLeggos) {
	let nbFingers = Object.keys(fingers).length;
	if (nbFingers < 2) {
		finalInnerHTML = 'You must provide at least 2 fingers or none.';
		return;
	}
	for (let finger1 = 0 ; finger1 < nbFingers ; finger1++) {
		for (let finger2 = finger1 + 1 ; finger2 < nbFingers ; finger2++) {
			let keyFinger1 = Object.keys(fingers)[finger1];
			let keyFinger2 = Object.keys(fingers)[finger2];
			if (fingers[keyFinger1].id != fingers[keyFinger2].id) {
				let localNbLeggos = nbLeggos;
				let localTitle = title;
				let localInnerHTML = innerHTML;	
				localNbLeggos += fingers[keyFinger1].legendary + fingers[keyFinger2].legendary;
				localTitle += '+' + keyFinger1 + '+' + keyFinger2;
				localInnerHTML += 'finger1=' + fingers[keyFinger1].string + '</br>';
				localInnerHTML += 'finger2=' + fingers[keyFinger2].string + '</br>';
				handleGear(0, localTitle, localInnerHTML, localNbLeggos);
			}			
		}
	}
	return;
}

function handleGear(depth, title, innerHTML, nbLeggos) {
	let nbGear = Object.keys(gear).length;
	if (depth >= nbGear) {
		finalInnerHTML += title + '</br>' + innerHTML + '</br>';
		return;
	}
	let keyType = Object.keys(gear)[depth];
	let nbGearType = Object.keys(gear[keyType]).length;
	for (let item = 0 ; item < nbGearType ; item++) {
		let localNbLeggos = nbLeggos;
		let localTitle = title;
		let localInnerHTML = innerHTML;	
		let keyItem = Object.keys(gear[keyType])[item];
		localNbLeggos += gear[keyType][keyItem].legendary;
		if (localNbLeggos <= 2) {
			localTitle += '+' + keyItem;
			localInnerHTML += keyType + '=' + gear[keyType][keyItem].string + '</br>';
			handleGear(depth + 1, localTitle, localInnerHTML, localNbLeggos);
		}
	}
	return;
}

function createListExport() {
	/// Creates a list of SimC commentaries that contains all the trinkets of the input so the user can paste it and save it somewhere for later use
	let chest = document.getElementById('chest').value;
	let nbTrinkets = Object.keys(trinkets).length;
	let nbFingers = Object.keys(fingers).length;
	let nbGear = Object.keys(gear).length;
	for (let i = 0 ; i < nbTrinkets ; i++) {
		let key = Object.keys(trinkets)[i];
		finalInnerHTML += '# ' + trinkets[key].string + ' # ' + key + '</br>';
	}
	if (nbTrinkets > 0) {
		finalInnerHTML += '</br>';
	}
	if (chest != '') {
		let karaChest = "karaChest" + selectValue(chest, 'ilevel');
		delete gear.chest[karaChest];
	}
	for (let i = 0 ; i < nbFingers ; i++) {
		let key = Object.keys(fingers)[i];
		finalInnerHTML += '# finger=' + fingers[key].string + ' # ' + key + '</br>';
	}
	for (let type = 0 ; type < nbGear ; type++) {
		let keyType = Object.keys(gear)[type];
		for (let item = 0 ; item < Object.keys(gear[keyType]).length ; item++) {
			let keyItem = Object.keys(gear[keyType])[item];
			finalInnerHTML += '# ' + gear[keyType][keyItem].string + ' # ' + keyItem + '</br>';
		}
	}
	return;
}

function restore() {
	loadCache();
	let trinketsReader = document.getElementById("trinkets");
	let gearReader = document.getElementById("gear");
	let trinketsInput = '';
	let gearInput = '';
	let nbTrinkets = Object.keys(trinkets).length;
	let nbFingers = Object.keys(fingers).length;
	let nbGear = Object.keys(gear).length;
	for (let i = 0 ; i < nbTrinkets ; i++) {
		let key = Object.keys(trinkets)[i];
		trinketsInput += '# ' + trinkets[key].string + ' # ' + key + '\n';
	}
	for (let i = 0 ; i < nbFingers ; i++) {
		let key = Object.keys(fingers)[i];
		gearInput += '# finger=' + fingers[key].string + ' # ' + key + '\n';
	}
	for (let type = 0 ; type < nbGear ; type++) {
		let keyType = Object.keys(gear)[type];
		for (let item = 0 ; item < Object.keys(gear[keyType]).length ; item++) {
			let keyItem = Object.keys(gear[keyType])[item];
			gearInput += '# ' + keyType + '=' + gear[keyType][keyItem].string + ' # ' + keyItem + '\n';
		}
	}
	trinketsReader.value = trinketsInput;
	gearReader.value = gearInput;
}

function saveCache() {
	if (Object.keys(trinkets).length !== 0 && trinkets.constructor === Object) {
		localStorage.setItem("trinkets", JSON.stringify(trinkets));
	}
	if (Object.keys(fingers).length !== 0 && fingers.constructor === Object) {
		localStorage.setItem("fingers", JSON.stringify(fingers));
	}
	if (Object.keys(gear).length !== 0 && gear.constructor === Object) {
		localStorage.setItem("gear", JSON.stringify(gear));
	}
}

function loadCache() {
	trinkets = JSON.parse(localStorage.getItem("trinkets"));
	fingers = JSON.parse(localStorage.getItem("fingers"));
	gear = JSON.parse(localStorage.getItem("gear"));
}
